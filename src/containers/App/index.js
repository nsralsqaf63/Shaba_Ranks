// Core
import React, {Component} from 'react';

// Instruments
import Styles from './styles.scss';

// state = {
//     query : 'test'
// };
//

export default class App extends Component {

    constructor() {
        super();

        this.getMovies = ::this._getMovies;
        this.handleTextChange = ::this._handleTextChange;
    }

    state = {
        inputValue: ''
    };

    _getMovies(event) {
        event.preventDefault();

        const {inputValue, data} = this.state;

        const api = `https://api.themoviedb.org/3/search/multi?api_key=d272326e467344029e68e3c4ff0b4059&language=ru-RU&query=${inputValue}`

        fetch(api, {method: 'GET'}).then((response) => {
            if (response.status !== 200) {
                throw new Error('Something go wrong');
            }

            return response.json()
        }).then((data) => {
            console.log(data);
        })
    }
    _handleTextChange(event) {
        const inputValue = event.target.value;

        this.setState(() => ({inputValue}));
    }

    render() {
        const {inputValue} = this.state;

        return (
            <section className={Styles}>
                <form onSubmit={this.getMovies}>
                    <input type="text" value={inputValue} onChange={this.handleTextChange}/>
                    <input type="submit" value="click"/>
                </form>

            </section>
        );
    }
}
